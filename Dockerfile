FROM scratch
COPY server /server
COPY config.yml /etc/testapp/config.yml
ENTRYPOINT ["/server"]
