package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

var (
	VersionString string = "development"
	value         string = "empty"
)

func handler(w http.ResponseWriter, r *http.Request) {
	hostname, _ := os.Hostname()
	fmt.Fprintf(w, "Hi there, here is %s in version %s!\n", hostname, VersionString)
	fmt.Fprintf(w, "Your Key is '%s'\n", value)
}

func main() {
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/testapp/")
	err := viper.ReadInConfig()
	if err != nil {
		log.Println("no confiugration found")
	} else {
		value = viper.GetString("key")
		viper.WatchConfig()
		viper.OnConfigChange(func(e fsnotify.Event) {
			value = viper.GetString("key")
		})
	}

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
