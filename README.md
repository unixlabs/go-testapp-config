# go-testapp-config
Test App with viper configuration written in Golang
# build
```
make all+push
```
# run in docker
```
docker run -d -p 8080:8080 registry.gitlab.com/unixlabs/go-testapp-config:3
```
# run in k8s
```
kubectl apply -f k8s.yml
```